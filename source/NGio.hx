package;

import flixel.FlxG;
import flixel.util.FlxSignal;
import flixel.util.FlxTimer;
import io.newgrounds.NG;
import io.newgrounds.components.ScoreBoardComponent.Period;
import io.newgrounds.objects.Medal;
import io.newgrounds.objects.Score;
import io.newgrounds.objects.ScoreBoard;
import io.newgrounds.objects.events.Response;
import io.newgrounds.objects.events.Result.GetCurrentVersionResult;
import io.newgrounds.objects.events.Result.GetVersionResult;
import lime.app.Application;
import openfl.display.Stage;

using StringTools;

/**
 * MADE BY GEOKURELI THE LEGENED GOD HERO MVP
 */
class NGio
{
	public static var isLoggedIn:Bool = false;
	public static var scoreboardsLoaded:Bool = false;

	public static var scoreboardArray:Array<Score> = [];

	public static var ngDataLoaded(default, null):FlxSignal = new FlxSignal();
	public static var ngScoresLoaded(default, null):FlxSignal = new FlxSignal();

	public static var GAME_VER:String = "";
	public static var GAME_VER_NUMS:String = '';
	public static var gotOnlineVer:Bool = false;

	public static function noLogin(api:String)
	{
		trace('NGIO NULLED');
	}

	public function new(api:String, encKey:String, ?sessionId:String)
	{
		trace('NGIO NULLED');
	}

	function onNGLogin():Void
	{
		trace('NGIO NULLED');
	}

	function onNGMedalFetch():Void
	{
		trace('NGIO NULLED');
	}

	function onNGBoardsFetch():Void
	{
		trace('NGIO NULLED');
	}

	inline static public function postScore(score:Int = 0, song:String)
	{
		trace('NGIO NULLED');
	}

	function onNGScoresFetch():Void
	{
		scoreboardsLoaded = false;

		trace('NGIO NULLED');
	}

	inline static public function logEvent(event:String)
	{
		trace('NGIO NULLED');
	}

	inline static public function unlockMedal(id:Int)
	{
		trace('NGIO NULLED');
	}
}
